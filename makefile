CC = gcc
FILES = main.c ext.c fat.c
HEADERS = ext.h fat.h structs_ext.h structs_fat.h global.h
PROGNAME = infinitywar

all: $(FILES) $(HEADERS)
	$(CC) $(FILES) -o $(PROGNAME) -lm

check: $(FILES) $(HEADERS)
	$(CC) $(FILES)

indent: $(FILES) $(HEADERS)
	indent $(FILES) $(HEADERS)

clean:
	rm -f *~
	rm -f *.plist
	rm -f $(PROGNAME)
	rm -f *core*
	rm -f *.html
	rm *.o
