#include "fat.h"

void goToInodeEntry(int fd, int inode, Super_block superblock);
__le64 getInodeSize(int fd, int inode, Super_block superblock);
char* getInodeCreationTime(int fd, int inode, Super_block superblock);
void exploreDataBlock(int fd, long inode, Super_block superblock, __le64 inodeSize, bool getInodeEntry);
void exploreEntry(int fd, Extent extent, Super_block superblock, char *filename, bool getInodeEntry);
void exploreInode(int fd, int inode, Super_block superblock, char *filename, bool getInodeEntry);
Super_block ext4_info(int fd);
void ext4_printInfo(Super_block superblock);
void ext4_searchfile(int fd, char *filename);
