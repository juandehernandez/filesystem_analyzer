#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>

#define __le64  u_int64_t
#define __le32	u_int32_t
#define __le16	u_int16_t
#define __le8   u_int8_t

typedef struct {
  __le32 s_inodes_count;     /* Inodes count */
  __le32 s_blocks_count;     /* Blocks count */
  __le32 s_r_blocks_count_lo;   /* Reserved blocks count */
  __le32 s_free_blocks_count;    /* Free blocks count */
  __le32 s_free_inodes_count;    /* Free inodes count */
  __le32 s_first_data_block; /* First Data Block */
  __le32 s_log_block_size;   /* Block size */
  __le32 s_log_cluster_size;    /* Cluster size */
  __le32 s_blocks_per_group;
  __le32 s_clusters_per_group;
  __le32 s_inodes_per_group;
  __le32 s_mtime;
  __le32 s_wtime;
  __le16 s_mnt_count;
  __le16 s_max_mnt_count;
  __le16 s_magic;
  __le16 s_state;
  __le16 s_errors;
  __le16 s_minor_rev_level;
  __le32 s_lastcheck;
  __le32 s_checkinterval;
  __le32 s_creator_os;
  __le32 s_rev_level;
  __le16 s_def_resuid;
  __le16 s_def_resgid;
  __le32 s_first_ino;
  __le16 s_inode_size;
  __le16 s_block_group_nr;
  __le32 s_feature_compat;
  __le32 s_feature_incompat;
  __le32 s_feature_ro_compat;
  char s_uuid[16];
  char s_volume_name[16];
  __le32 s_r_blocks_count_hi;
} Super_block;

typedef struct {
  __le16 magic;
  __le16 entries;
  __le16 max;
  __le16 depth;
  __le32 generation;
} Extent_header;

typedef struct {
  __le32 block;
  __le16 len;
  __le16 start_hi;
  __le32 start_lo;
  __le64 start;
} Extent;

typedef struct {
  __le32 block;
  __le32 leaf_lo;
  __le16 leaf_hi;
  __le16 unused;
  __le64 leaf;
} Extent_idx;

typedef struct {
  __le32 inode;
  __le16 dir_length;
  __le8 filename_length;
  __le8 file_type;
  char *filename;
} Dir_entry;
