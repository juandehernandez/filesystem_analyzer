#include "ext.h"

#define NUM_ARG 2
#define NUM_ARG_INFO 3
#define NUM_ARG_SEARCH 4

#define ERR_ARG "Error de argumentos.\n"
#define ERR_OP "Operación no permitida.\n"
#define FILE_NOT_FOUND "\nError. File not found.\n"
#define FILESYSTEM_NOT_FOUND "\nError. Filesystem not found.\n"

bool fileFound = false;
int fileInode = -1;
FAT_entry fileEntry;

int main (int argc, char *argv[] ){
  //Comprobamos que almenos hay un mínimo de parámetros (mínimo operación -info o -search)
  if (argc < NUM_ARG) {
    write(1, ERR_ARG, sizeof(ERR_ARG));
    return 1;
  }

  //Comprobamos que parametro 1 es operacion permitida: -info o -search
  if (strcmp(argv[1], "-info") != 0
  && strcmp(argv[1], "-search") != 0
  && strcmp(argv[1], "-show") != 0) {
    write(1, ERR_OP, sizeof(ERR_OP));
    return 1;
  }

  //Comprobamos que si es una operación permitida,
  //tenga el número de parámetros que le corresponden
  if ((strcmp(argv[1], "-info") == 0 && argc != NUM_ARG_INFO)
  || (strcmp(argv[1], "-search") == 0 && strcmp(argv[1], "-show") == 0 && argc != NUM_ARG_SEARCH)) {
    write(1, ERR_ARG, sizeof(ERR_ARG));
    return 1;
  }


  int fd;
  fd = open(argv[2], O_RDONLY);

  if (fd < 0) {
    write(1, FILESYSTEM_NOT_FOUND, sizeof(FILESYSTEM_NOT_FOUND));
    return 1;
  }

  //First wee need to check the type of the volume
  //To check it, we can see the Magic signature in superblock (s_magic)
  //The magic signature is only for volumes of type ext.
  //Volumes of type fat have 0 bits at the same position.
  __le16 magic_number;
  lseek(fd, 0x438, SEEK_SET);
  read(fd, &magic_number, sizeof(magic_number));
  if (magic_number == 61267) {
    //Volume is type ext

    //Now we will check if volume is ext2, or is ext3 or ext4
    //We can see if volume ext has journal or not
    //If not, then volume is ext2 and if yes, volume is ext3 or ext4
    __le32 s_feature_compat;
    lseek(fd, 0x45C, SEEK_SET);
    read(fd, &s_feature_compat, sizeof(s_feature_compat));
    if (((s_feature_compat >> 2)  & 0x01) == 1) {
      //Volume is ext3 or ext4

      //Now we will read the next 4 bytes in the same variable.
      //The next 4 bytes is s_feature_incompat (last 4 bytes was s_feature_compat)
      read(fd, &s_feature_compat, sizeof(s_feature_compat));
      if (((s_feature_compat >> 9)  & 0x01) == 1) {
        Super_block superblock = ext4_info(fd);
        if (strcmp(argv[1], "-info") == 0) {
          ext4_printInfo(superblock);
        } else if (strcmp(argv[1], "-search") == 0){
          ext4_searchfile(fd, argv[3]);
          if (fileFound) {
            //Leemos el size del inodo por si encontramos el fichero más tarde. (File Size)
            __le64 inodeSize = getInodeSize(fd, fileInode, superblock);

            //Leemos el creation time del inodo
            char *creationTime = getInodeCreationTime(fd, fileInode, superblock);
            printf("\nFile found! Size: %llu byte. Created on: %s", inodeSize, creationTime);

          } else {
            write(1, FILE_NOT_FOUND, strlen(FILE_NOT_FOUND));
          }
        } else if (strcmp(argv[1], "-show") == 0) {
          ext4_searchfile(fd, argv[3]);
          //fileInode is a global variable set when file is found
          //Leemos el size del inodo por si encontramos el fichero más tarde. (File Size)
          __le64 inodeSize = getInodeSize(fd, fileInode, superblock);
          exploreDataBlock(fd, fileInode, superblock, inodeSize, true);
        }
      } else {
        //Volume is ext3
        printf("File System not recognized (EXT3)\n");
      }
    } else {
      //Volume is ext2
      printf("File System not recognized (EXT2)\n");

    }

  } else {
    //Volume is type fat
    __le16 sectors_per_fat;
    lseek(fd, 0x16, SEEK_SET);
    read(fd, &sectors_per_fat, sizeof(sectors_per_fat));
    if (sectors_per_fat == 0) {
      //Volume is fat32
      if (strcmp(argv[1], "-info") == 0) {
        fat32_info(fd);
      } else if (strcmp(argv[1], "-search") == 0){
        fat32_searchfile(fd, argv[3]);
        if (fileFound) {
          int day = fileEntry.file_creation_date & 0x001F;
          int month = (fileEntry.file_creation_date >> 5) & 0x000F;
          int year = (fileEntry.file_creation_date >> 9) + 1980;
          printf("\nFile found! Size: %u byte. Created on: %d/%d/%d\n", fileEntry.file_size, day, month, year);
        } else {
          write(1, FILE_NOT_FOUND, strlen(FILE_NOT_FOUND));
        }
      } else if (strcmp(argv[1], "-show") == 0) {
        BPIOS bpios = fat32_info(fd);
        fat32_searchfile(fd, argv[3]);
        exploreDataCluster(fd, fileEntry, bpios);
      }
    } else {
      //Volume is fat16
      printf("File System not recognized (FAT16)\n");
    }

  }

  return 0;
}
