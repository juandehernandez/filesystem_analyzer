#include "ext.h"

void goToInodeEntry(int fd, int inode, Super_block superblock) {
  //Nos ponemos a leer block group descriptor
  //Nos ponemos en dirección para leer localización de la tabla de inodos
  //Primero leemos los 4 bytes de la parte baja
  int offset = 0;
  if (superblock.s_log_block_size == 1024) {
    offset = 1024;
  }
  lseek(fd, offset + 0x08 + superblock.s_log_block_size, SEEK_SET);
  __le32 location_inode_table_low;
  read(fd, &location_inode_table_low, sizeof(location_inode_table_low));
  //Ahora leemos los 4 bytes de la parte alta
  lseek(fd, offset + 0x28 + superblock.s_log_block_size, SEEK_SET);
  __le32 location_inode_table_high;
  read(fd, &location_inode_table_high, sizeof(location_inode_table_high));

  __le64 value = mergeValues(location_inode_table_high, location_inode_table_low);

  int bytePositionInode = ((inode - 1) % superblock.s_inodes_per_group) * superblock.s_inode_size;

  lseek(fd, value * superblock.s_log_block_size + bytePositionInode, SEEK_SET);

}

__le64 getInodeSize(int fd, int inode, Super_block superblock) {
  //Nos ponemos en la posición del inode entry correspondiente en la inode table
  goToInodeEntry(fd, inode, superblock);

  //Nos ponemos en el offset para leer la parte baja del tamaño del archivo
  lseek(fd, 0x4, SEEK_CUR);
  __le32 fileSize_lo;
  read(fd, &fileSize_lo, sizeof(fileSize_lo));
  //Ahora parte alta
  lseek(fd, 0x6C, SEEK_CUR);
  __le32 fileSize_hi;
  read(fd, &fileSize_hi, sizeof(fileSize_hi));
  __le64 fileSize = mergeValues(fileSize_hi, fileSize_lo);

  return fileSize;
}

char* getInodeCreationTime(int fd, int inode, Super_block superblock) {
  //Nos ponemos en la posición del inode entry correspondiente en la inode table
  goToInodeEntry(fd, inode, superblock);

  //Nos ponemos en el offset para leer la fecha de creación del inodo
  lseek(fd, 0x90, SEEK_CUR);
  __le32 creationTime;
  read(fd, &creationTime, sizeof(creationTime));

  time_t time = (time_t)(creationTime);
  char *crTime = ctime(&time);

  return crTime;
}

void exploreDataBlock(int fd, long inode, Super_block superblock, __le64 inodeSize, bool getInodeEntry) {
  if (getInodeEntry) {
    //Nos situamos en la posición del inode entry con lseeks en la función siguiente
    goToInodeEntry(fd, inode, superblock);
    //Nos hemos puesto en el bloque de la entrada del inodo en la tabla de inodos
    //pero queremos ir al extent tree (offset 0x28)
    lseek(fd, 0x28, SEEK_CUR);
  } else {
    lseek(fd, superblock.s_log_block_size * inode, SEEK_SET);
  }

  Extent_header extent_header;
  read(fd, &extent_header, sizeof(extent_header));


  if (extent_header.depth == 0) {


    for (int i = 0; i < extent_header.entries; i++) {
      Extent extent;
      read(fd, &extent.block, 4);
      read(fd, &extent.len, 2);
      read(fd, &extent.start_hi, 2);
      read(fd, &extent.start_lo, 4);

      off_t file_byte_position = lseek(fd, 0, SEEK_CUR);

      extent.start = mergeValues(extent.start_hi, extent.start_lo);



      lseek(fd, extent.start * superblock.s_log_block_size, SEEK_SET);

      off_t myaux = lseek(fd, 0, SEEK_CUR);


      for (unsigned long j = 0; j < extent.len * superblock.s_log_block_size; j++) {
        char aux;
        read(fd, &aux, sizeof(char));
        write(1, &aux, sizeof(char));
      }
      printf("\n");

      // Volvemos a la posicion de antes
      lseek(fd, file_byte_position, SEEK_SET);
    }

  } else {


    for (int i = 0; i < extent_header.entries; i++) {
        Extent_idx extent_idx;
        read(fd, &extent_idx.block, 4);
        read(fd, &extent_idx.leaf_lo, 4);
        read(fd, &extent_idx.leaf_hi, 2);
        read(fd, &extent_idx.unused, 2);


        off_t file_byte_position = lseek(fd, 0, SEEK_CUR);

        extent_idx.leaf = mergeValues(extent_idx.leaf_hi, extent_idx.leaf_lo);

        exploreDataBlock(fd, extent_idx.leaf, superblock, inodeSize, false);

        lseek(fd, file_byte_position, SEEK_SET);
    }
  }

}

void exploreEntry(int fd, Extent extent, Super_block superblock, char *filename, bool getInodeEntry) {
  lseek(fd, extent.start * superblock.s_log_block_size, SEEK_SET);
  while (1) {
    Dir_entry direntry;
    off_t file_byte_position;
    //Leemos 8 bytes
    //(excepto filename porque es un puntero a carácteres y tendremos que hacer malloc)
    read(fd, &direntry, sizeof(__le64));

    if (direntry.inode == 0) {
      break;
    }
    //Hacemos malloc para el filename
    direntry.filename = malloc(sizeof(char) * (direntry.filename_length + 1));
    read(fd, direntry.filename, direntry.filename_length * sizeof(char));
    direntry.filename[direntry.filename_length] = '\0';

    lseek(fd, direntry.dir_length - direntry.filename_length - sizeof(__le64), SEEK_CUR);

    //Guardamos la posición donde nos encontramos
    //para después de ejecutar exploreInode saber donde nos encontramos
    file_byte_position = lseek(fd, 0, SEEK_CUR);

    if (direntry.file_type == 0x1 && direntry.filename[0] != '.') {
      //File Type is REGULAR FILE
      if (strcmp(direntry.filename, filename) == 0) {
        fileFound = true;
        fileInode = direntry.inode;
      }

    } else if (direntry.file_type == 0x2 && direntry.filename[0] != '.') {
      //File Type is DIR
      exploreInode(fd, direntry.inode, superblock, filename, getInodeEntry);
    }
    //Volvemos a la posición de antes y liberamos la memoria dinámica generada
    lseek(fd, file_byte_position, SEEK_SET);
    free(direntry.filename);
  }
}

void exploreInode(int fd, int inode, Super_block superblock, char *filename, bool getInodeEntry) {
  off_t file_byte_position;

  if (getInodeEntry) {
    //Nos situamos en la posición del inode entry con lseeks en la función siguiente
    goToInodeEntry(fd, inode, superblock);

    //Nos hemos puesto en el bloque del inodo pero queremos ir al extent tree (offset 0x28)
    lseek(fd, 0x28, SEEK_CUR);
  } else {
    //En este caso venimos de un internal node y directamente vamos a su arbol

    //Inode seria el numero de bloque a llegar pero no realmente el numero de inodo (Aprovechamos variable)
    lseek(fd, superblock.s_log_block_size * inode, SEEK_SET);

  }

  Extent_header extent_header;
  read(fd, &extent_header, sizeof(extent_header));

  if (extent_header.depth == 0) {

    for (int i = 0; i < extent_header.entries; i++) {
      Extent extent;
      read(fd, &extent.block, 4);
      read(fd, &extent.len, 2);
      read(fd, &extent.start_hi, 2);
      read(fd, &extent.start_lo, 4);

      off_t file_byte_position = lseek(fd, 0, SEEK_CUR);

      extent.start = mergeValues(extent.start_hi, extent.start_lo);
      exploreEntry(fd, extent, superblock, filename, getInodeEntry);

      // Volvemos a la posicion de antes
      lseek(fd, file_byte_position, SEEK_SET);
    }

  } else {
    printf("Other extent inodes\n");
    //Other extent inodes

    for (int i = 0; i < extent_header.entries; i++) {
        Extent_idx extent_idx;
        read(fd, &extent_idx.block, 4);
        read(fd, &extent_idx.leaf_lo, 4);
        read(fd, &extent_idx.leaf_hi, 2);
        read(fd, &extent_idx.unused, 2);

        off_t file_byte_position = lseek(fd, 0, SEEK_CUR);
        extent_idx.leaf = mergeValues(extent_idx.leaf_hi, extent_idx.leaf_lo);
        exploreInode(fd, extent_idx.leaf, superblock, filename, false);
        lseek(fd, file_byte_position, SEEK_SET);

    }



  }

}

Super_block ext4_info(int fd) {
  //Leemos parte del booter
  lseek(fd, 0x400, SEEK_SET);
  //Leemos superbloque
  Super_block superblock;
  read(fd, &superblock, sizeof(superblock));

  //Leemos la parte alta de reserved block count
  lseek(fd, 0x400 + 0x150, SEEK_SET);
  read(fd, &superblock.s_r_blocks_count_hi, sizeof(superblock.s_r_blocks_count_hi));

  //Calculamos el block size haciendo el pow
  //y lo reasignamos a la misma variable del superblock
  superblock.s_log_block_size = (int) pow(2, superblock.s_log_block_size + 10);

  return superblock;

}

void ext4_printInfo(Super_block superblock) {
  printf("\n---- Filesystem Information ----\n");
  printf("\nFilesystem: EXT4\n");
  printf("\nINODE INFO\n");
  printf("Inode Size: %d\n", superblock.s_inode_size);
  printf("Number of Inodes: %d\n", superblock.s_inodes_count);
  printf("First Inode: %d\n", superblock.s_first_ino);
  printf("Inodes Group: %d\n", superblock.s_inodes_per_group);
  printf("Free Inodes: %d\n", superblock.s_free_inodes_count);

  printf("\nBLOCK INFO\n");
  printf("Block Size: %d\n", superblock.s_log_block_size);
  printf("Reserved Blocks: %llu\n", mergeValues(superblock.s_r_blocks_count_hi, superblock.s_r_blocks_count_lo));
  printf("Free Blocks: %d\n", superblock.s_free_blocks_count);
  printf("Total Blocks: %d\n", superblock.s_blocks_count);
  printf("First Block: %d\n", superblock.s_first_data_block);
  printf("Block group: %d\n", superblock.s_blocks_per_group);
  printf("Frags Group: %d\n ", superblock.s_clusters_per_group);

  printf("\nVOLUME INFO\n");
  printf("Volume name: %s\n", superblock.s_volume_name);
  time_t time = (time_t)(superblock.s_lastcheck);
  char *lastcheck = ctime(&time);
  printf("Last check: %s", lastcheck);
  time = (time_t)(superblock.s_mtime);
  char *mtime = ctime(&time);
  printf("Last mount: %s", mtime);
  time = (time_t)(superblock.s_wtime);
  char *wtime = ctime(&time);
  printf("Last written: %s", wtime);
}

void ext4_searchfile(int fd, char *filename) {
  Super_block superblock = ext4_info(fd);

  //Exploramos inodo en busca del archivo
  exploreInode(fd, 2, superblock, filename, true);
}
