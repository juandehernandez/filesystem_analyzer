#include "structs_ext.h"

typedef struct {
  char filename[11];
  __le8 attributes;
  __le8 reserved;
  __le8 creation_time_tenths_of_second;
  __le16 file_creation_time;
  __le16 file_creation_date;
  __le16 last_access_date;
  __le16 first_cluster_number_high;
  __le16 last_modification_time;
  __le16 last_modification_date;
  __le16 first_cluster_number_low;
  __le32 file_size;
} FAT_entry;

typedef struct {
  __le8 order;
  unsigned char first_5[10];
  __le8 attr;
  __le8 long_type;
  __le8 checksum;
  unsigned char seg_6[12];
  unsigned char always_zero[2];
  unsigned char final_2[4];
} FAT_longname_entry;

typedef struct {
  char system_name[8];
  __le16 bytes_per_sector;
  __le8 sectors_per_cluster;
  __le16 reserved_sectors;
  __le8 fats;
  __le16 dir_entries_count;
  __le32 sectors_per_fat32;
  __le16 flags;
  char volume_label_string[11];
  __le32 cluster_root;
  __le32 entries_per_sector;
} BPIOS;
