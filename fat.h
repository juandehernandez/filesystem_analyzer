#include "global.h"

void fat32_printInfo(BPIOS bpios);
BPIOS fat32_info(int fd);
unsigned int nextCluster(int fd, BPIOS bpios, int active_cluster);
unsigned int getFirstClusterSector(BPIOS bpios, unsigned int cluster);
void exploreDataCluster(int fd, FAT_entry entry, BPIOS bpios);
void exploreCluster(int fd, BPIOS bpios, int cluster, char *filename);
void fat32_searchfile(int fd, char *filename);
__le64 mergeValues(__le32 high, __le32 low);
