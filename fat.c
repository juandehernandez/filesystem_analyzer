#include "fat.h"

__le64 mergeValues(__le32 high, __le32 low) {
  unsigned long value = 0;

  value = ((unsigned long)high << 32);
  value |= low;

  return value;
}

void fat32_printInfo(BPIOS bpios) {
  printf("\n---- Filesystem Information ----\n");
  printf("\nFilesystem: FAT32\n\n");
  printf("System Name: %s\n", bpios.system_name);
  printf("Sector size: %d\n", bpios.bytes_per_sector);
  printf("Sectors per Cluster: %d\n", bpios.sectors_per_cluster);
  printf("Reserved sectors: %d\n", bpios.reserved_sectors);
  printf("Number of FATs: %d\n", bpios.fats);
  printf("Maximum Root Entries: %d\n", bpios.dir_entries_count);
  printf("Sectors per FAT: %d\n", bpios.sectors_per_fat32);
  printf("Label: %s\n", bpios.volume_label_string);
}

BPIOS fat32_info(int fd) {

  BPIOS bpios;

  //Get System Name
  lseek(fd, 0x03, SEEK_SET);
  read(fd, &bpios.system_name, sizeof(bpios.system_name));

  //Number of File Allocation Tables (FAT's) on the storage media.
  lseek(fd, 0x10, SEEK_SET);
  read(fd, &bpios.fats, sizeof(bpios.fats));

  //Get sector size
  lseek(fd, 0x0B, SEEK_SET);
  read(fd, &bpios.bytes_per_sector, sizeof(bpios.bytes_per_sector));

  //Get number of reserved sectors
  lseek(fd, 0x0D, SEEK_SET);
  read(fd, &bpios.sectors_per_cluster, sizeof(bpios.sectors_per_cluster));

  //Get number of reserved sectors
  lseek(fd, 0x0E, SEEK_SET);
  read(fd, &bpios.reserved_sectors, sizeof(bpios.reserved_sectors));

  //Get maximum root entries
  lseek(fd, 0x11, SEEK_SET);
  read(fd, &bpios.dir_entries_count, sizeof(bpios.dir_entries_count));

  //Get number of sectors per fat32
  lseek(fd, 0x024, SEEK_SET);
  read(fd, &bpios.sectors_per_fat32, sizeof(bpios.sectors_per_fat32));

  //Get volume label string
  lseek(fd, 0x047, SEEK_SET);
  read(fd, &bpios.volume_label_string, sizeof(bpios.volume_label_string));

  //Get the cluster number of the root directory
  lseek(fd, 0x02C, SEEK_SET);
  read(fd, &bpios.cluster_root, sizeof(bpios.cluster_root));

  //Calculate entries per sector
  bpios.entries_per_sector = ((bpios.bytes_per_sector * bpios.sectors_per_cluster) / 32);

  return bpios;
}

unsigned int nextCluster(int fd, BPIOS bpios, int active_cluster) {
  int sector_size = bpios.bytes_per_sector;
  unsigned char FAT_table[sector_size];
  unsigned int fat_offset = active_cluster * 4;
  unsigned int fat_sector = bpios.reserved_sectors + (fat_offset / sector_size);
  unsigned int ent_offset = fat_offset % sector_size;


  //at this point you need to read from sector "fat_sector" on the disk into "FAT_table".
  lseek(fd, fat_sector * sector_size, SEEK_SET);
  read(fd, FAT_table, sector_size * sizeof(char));

  //remember to ignore the high 4 bits.
  unsigned int table_value = *(unsigned int*)&FAT_table[ent_offset] & 0x0FFFFFFF;

  return table_value;
}

unsigned int getFirstClusterSector(BPIOS bpios, unsigned int cluster) {
  unsigned int fatSize = bpios.sectors_per_fat32;
  unsigned int firstDataSector = bpios.reserved_sectors + (bpios.fats * fatSize);
  unsigned int clusterSector = ((cluster - 2) * bpios.sectors_per_cluster) + firstDataSector;

  return clusterSector;
}

void exploreDataCluster(int fd, FAT_entry entry, BPIOS bpios) {
  int cluster = mergeValues(entry.first_cluster_number_high, entry.first_cluster_number_low);
  int clusterSector = getFirstClusterSector(bpios, cluster);

  unsigned long clusterSize = (unsigned long) bpios.bytes_per_sector * bpios.sectors_per_cluster;
  unsigned long bytesToRead;
  if (entry.file_size > clusterSize) bytesToRead = clusterSize;
  else bytesToRead = entry.file_size;

  while (1) {
    lseek(fd, clusterSector * bpios.bytes_per_sector, SEEK_SET);

    for (int i = 0; i < bytesToRead; i++) {
      char aux;
      read(fd, &aux, sizeof(char));
      write(1, &aux, sizeof(char));
    }

    entry.file_size = entry.file_size - clusterSize;
    if (entry.file_size > clusterSize) bytesToRead = clusterSize;
    else bytesToRead = entry.file_size;

    unsigned int next_cluster = nextCluster(fd, bpios, cluster);
    if (next_cluster < 0xFFF7) {
      clusterSector = next_cluster;
    } else {
      break;
    }
  }
}

void exploreCluster(int fd, BPIOS bpios, int cluster, char *filename) {
  int clusterSector = getFirstClusterSector(bpios, cluster);
  char longnames[50][14];
  int count_longnames = 0;
  while(1) {
    lseek(fd, clusterSector * bpios.bytes_per_sector, SEEK_SET);

    FAT_entry entry;

    for (int i = bpios.entries_per_sector; i > 0; i--) {

      __le8 byte;
      read(fd, &byte, sizeof(byte));

      if (byte == 0) {
        break;
      } else if (byte == 0xE5 || byte == 0x2E) {
        lseek(fd, -1, SEEK_CUR);
        read(fd, &entry, sizeof(entry));
        continue;
      }

      lseek(fd, 10, SEEK_CUR);
      read(fd, &byte, sizeof(byte));

      lseek(fd, -12, SEEK_CUR);

      //Vamos a comprobar si el byte número 11 es una entrada long file name
      if (byte == 0x0F) {
        //Si es una entrada de long file name,
        //entonces leeremos la porción long filename en un buffer temporal.
        FAT_longname_entry longname_entry;
        read(fd, &longname_entry, sizeof(longname_entry));

        char longname[14];

        int j = 0;
        int k = 0;
        while (j < 10) {
          if (longname_entry.first_5[j] != 0x00
            && longname_entry.first_5[j] != 0xFF) longname[k++] = longname_entry.first_5[j];
          j = j + 2;
        }
        j = 0;
        while (j < 12) {
          if (longname_entry.seg_6[j] != 0x00
            && longname_entry.seg_6[j] != 0xFF) longname[k++] = longname_entry.seg_6[j];
          j = j + 2;
        }
        j = 0;
        while (j < 4) {
          if (longname_entry.final_2[j] != 0x00
            && longname_entry.final_2[j] != 0xFF) longname[k++] = longname_entry.final_2[j];
          j = j + 2;
        }

        longname[k] = '\0';

        strcpy(longnames[count_longnames++], longname);

      } else {

        read(fd, &entry, sizeof(entry));

        char *fullname;
        if (count_longnames == 0) {
          char fullname[12];
          bzero(fullname, sizeof(char) * 12);

          int j = 0;
          while (entry.filename[j] != ' ') {
            fullname[j] = entry.filename[j];
            j++;
          }

          fullname[j++] = '.';

          int p = 8;
          while (entry.filename[p] != ' ') {
            fullname[j++] = entry.filename[p++];
          }

        } else {
          //If there are longnames entries on buffer
          int totalLength = 0;
          for (int j = 0; j < count_longnames; j++) {
            totalLength = totalLength + strlen(longnames[j]);
          }

          fullname = (char *) malloc(sizeof(char) * (totalLength + 1));
          bzero(fullname, sizeof(char) * (totalLength + 1));

          for (int j = count_longnames - 1; j >= 0; j--) {
            strcat(fullname, longnames[j]);
            bzero(longnames[j], strlen(longnames[j]));
          }
        }

        if (entry.attributes == 0x20) {
          //FILE
          if (strcmp(fullname, filename) == 0) {
            fileFound = true;
            fileEntry = entry;

          }
        } else if (entry.attributes == 0x10) {
          //DIRECTORY
          off_t file_byte_position = lseek(fd, 0, SEEK_CUR);
          unsigned int first_cluster_number = mergeValues(entry.first_cluster_number_high, entry.first_cluster_number_low);
          exploreCluster(fd, bpios, first_cluster_number, filename);
          lseek(fd, file_byte_position, SEEK_SET);
        }

        count_longnames = 0;

      }
    }

    unsigned int next_cluster = nextCluster(fd, bpios, cluster);

    if (next_cluster < 0xFFF7) {
      clusterSector = next_cluster;
    } else {
      break;
    }

  }
}

void fat32_searchfile(int fd, char *filename) {
  BPIOS bpios = fat32_info(fd);

  exploreCluster(fd, bpios, bpios.cluster_root, filename);

}
